function proximoDia (dia, mes, ano){
  mes.toLowerCase();
  
    if (mes === 'janeiro'){
      if (dia > 0 && dia < 30){
        dia += 1;
      } else if (dia > 30){
        mes = 'fevereiro';
        dia = 1;
      }   
  } else if (mes === 'fevereiro'){
      if (dia > 0 && dia < 28){
        dia += 1;
      } else if (dia >= 28){
        dia = 1;
        mes = 'março';
      }
  } else if (mes === 'março'){
    if (dia > 0  && dia < 31){
      dia += 1;
    } else if (dia >= 31){
      mes = 'abril';
      dia = 1;
    }  
  } else if (mes === 'abril'){
    if (dia > 0 && dia < 30){
      dia += 1;
    } else if (dia >= 30){
      mes = 'maio';
      dia = 1;
    }  
  } else if (mes === 'maio'){
    if(dia > 0 && dia < 31){
      dia += 1;
    } else if (dia >= 31){
      mes = 'junho';
      dia = 1;
    }   
  } else if (mes === 'junho'){
    if (dia > 0  && dia > 30){
      dia += 1;
    } else if (dia >= 30){
      mes = 'julho';
      dia = 1;
    }  
  } else if (mes === 'julho'){
    if(dia > 0 && dia < 31){
      dia += 1;
    } else if (dia >= 31){
      mes = 'agosto';
      dia = 1;
    } 
  } else if (mes === 'agosto'){
    if(dia > 0 && dia < 31){
      dia += 1;
    } else if (dia >= 31){
      mes = 'setembro';
      dia = 1;
    } 
  } else if (mes === 'setembro'){
    if (dia > 0  && dia > 30){
      dia += 1;
    } else if (dia >= 30){
      mes = 'outubro';
      dia = 1;
    }  
  } else if (mes === 'outubro'){
    if(dia > 0 && dia < 31){
      dia += 1;
    } else if (dia >= 31){
      mes = 'novembro';
      dia = 1;
    } 
  } else if (mes === 'novembro'){
    if(dia > 0 && dia < 30){
      dia += 1;
    } else if (dia >= 30){
      mes = 'dezembro';
      dia = 1;
    } 
  } else if (mes === 'dezembro'){
    if(dia > 0 && dia < 31){
      dia += 1;
    } else if (dia >= 31){
      mes = 'janeiro';
      dia = 1;
    } 
  } else {
    dia = 'dia inválido';
  } 
  return `${dia}/${mes}/${ano}`
}


console.log(proximoDia(3, 'novembro', 2020));