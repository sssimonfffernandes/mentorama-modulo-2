function reconhecerTriangulo(ladoA, ladoB, ladoC){
  let resposta = '';
  if (ladoA === 0 || ladoB === 0 || ladoC === 0){
    resposta = 'Não é um triângulo'
  } else if(ladoA === ladoB && ladoB === ladoC){
    resposta = 'Equilátero';
  } else if(ladoA === ladoB && ladoB != ladoC){
    resposta = 'Isósceles';
  } else if (ladoA != ladoB && ladoB === ladoC){
    resposta = 'Isósceles';
  } else if (ladoA === ladoC && ladoC != ladoB){
    resposta = 'Isósceles';
  } else if (ladoA != ladoC && ladoC === ladoB){
    resposta = 'Isósceles';
  } else if (ladoA != ladoB != ladoC){
    resposta = 'Escaleno';
  }  
  return resposta;
}

console.log(reconhecerTriangulo(30, 10, 20));