let valorA = prompt ( "Digite o valor de A:" );
let valorB = prompt ( "Digite o valor de B:" );
let valorC = prompt ( "Digite o valor de C:" );
let coeficiente1;
let coeficiente2;


let delta = (valorB * valorB) - 4 * valorA * valorC;

console.log(`Valor de Delta ${delta}`);

if (delta < 0) {
	console.log("Não existem raizes reais para Delta negativo");
} else {
	console.log("Raízes diferentes para delta positivo");

	coeficiente1 = (-valorB + Math.sqrt(delta)) / (2 * valorA);

coeficiente2 = (-valorB - Math.sqrt(delta)) / (2 * valorA);

  

 console.log("x' = " + coeficiente1);

  console.log("x'' = " + coeficiente2);

}
