function multiplicar(num){
    return num * 2;
}

multiplicar(56);

function dividir(num){
    return num / 2;
}

dividir(112);

function nomeSobrenome(nome, sobrenome){
    return console.log(`${nome} ${sobrenome}`);
}

nomeSobrenome('Simon', 'Fernandes');


function comparar(numA, numB){
    if (numA > numB){
        return 'Maior';
    } if (numA < numB){
        return 'Menor';
    } if (numA === numB){
        return 'Igual';
    }
}

comparar(26, 59);


function conversor(celsius){
    fahrenheit = celsius * 1.8 + 32;
	return fahrenheit;
}

conversor(26);
